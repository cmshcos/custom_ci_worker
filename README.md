# HCAL Custom worker image for GitLab-CI at CERN

This project builds docker images based on the CC7/8 cern containers.

The images contain all the necessary RPM dependencies to build HCAL XDAQ releases:
  * XDAQ `cmsos_core cmsos_worksuite`
  * sysmgr
  * uhal
  * AMC13
  * hcal-root

## Local use
Pull from the gitlab image Repository

`$ podman pull gitlab-registry.cern.ch/cmshcos/custom_ci_worker:cc7_xdaq15`

And now you can build inside the container:
```
$ podman run -it custom_ci_worker:cc7_xdaq15 bash
$ git clone ssh://git@gitlab.cern.ch:7999/cmshcos/hcal.git
$ cd hcal/hcalBase
$ make
``` 

## Checking package versions
You can check the contents of an image by running:

`podman run -it custom_ci_worker:cc7_xdaq15 bash; rpm -qa | grep cmsos`