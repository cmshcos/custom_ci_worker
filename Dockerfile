# Latest HCAL profile from P5, with working repos
# Centos 7.9 (rolling based on latest from cmsos internal build)
# Image provided by cmsos XDAQ working group

FROM gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-15-cc7-x86_64-hcal:latest

RUN \
      # Replace internal centos repos with ones that work
      yum-config-manager --disable \* && \
      yum-config-manager --add-repo https://linuxsoft.cern.ch/cern/centos/7/os/x86_64/ && \
      yum-config-manager --add-repo https://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/ && \
      yum-config-manager --add-repo https://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/ && \
      yum-config-manager --add-repo https://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/ && \
      # install pip and python-gitlab package for cc7
      rm -rf /etc/yum.repos.d/epel.* && \
      yum reinstall -y epel-release && \
      yum install -y python3-devel python3-setuptools && \
      # reduce image size by removing all cache, temp files etc.
      yum -y clean all --enablerepo='*'
